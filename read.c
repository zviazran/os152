#include "types.h"
#include "stat.h"
#include "user.h"

#define HowMachToReadAndWrite 1 

char buf[512];

int main(int argc, char *argv[]){
	int n=1;

	while((n = read(0, buf, HowMachToReadAndWrite)) > 0){
		if(buf[0]=='q'){
			n = read(0, buf, sizeof(buf));
			write(1, "\n", HowMachToReadAndWrite);
			break;
		}
		write(1, buf, HowMachToReadAndWrite);
	}
	if(n < 0){
		printf(1, "read: error\n");
		exit(1);
	}
	exit(0);
}