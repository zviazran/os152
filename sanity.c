#include "types.h"
#include "stat.h"
#include "user.h"

#define MAX_CPROC 20
#define MAX_WORK_CYCLE 10000000

struct  procInfo{
	int pid;
	int wtime;
	int rtime;
	int iotime;
};

int main(int argc, char *argv[]){
	int i, sum = 1;
	int childPid, pid;
	int waitCounter = 0, runCounter = 0, turnaroundCounter = 0;

	
	struct procInfo procInfo[MAX_CPROC];
	
	for(childPid = 0; childPid < MAX_CPROC; childPid++){
		if(!(pid = fork())){ 
			// child priority according to creation index
			set_priority(1+(childPid%3));
			for(i = 1; i < MAX_WORK_CYCLE; i++)
				sum = sum + sum;
			exit(pid);
		}
		else{ // the parent saves the childs process id
			  procInfo[childPid].pid = pid;
		}
	}
	set_priority(1);
	// validate exit status for every finished child
	for(i = 0; i < MAX_CPROC; i++){
		pid = wait_stat(&procInfo[i].wtime, &procInfo[i].rtime, &procInfo[i].iotime);
		if(procInfo[i].pid == pid){
			printf(1, "process number - %d\nWaiting time - %d\nRunning time - %d\nTurnaround time - %d\n\n",
			i, procInfo[i].wtime, procInfo[i].rtime, procInfo[i].wtime+procInfo[i].rtime+procInfo[i].iotime);
		
			waitCounter += procInfo[i].wtime;
			runCounter += procInfo[i].rtime;
			turnaroundCounter += (procInfo[i].wtime + procInfo[i].rtime + procInfo[i].iotime);
		}
	}
    
	printf(1, "Average waiting time is - %d\nAverage running time is - %d\nAverage turnaround time is - %d\n",
		(int)(waitCounter/MAX_CPROC),
		(int)(runCounter/MAX_CPROC),
		(int)(turnaroundCounter/MAX_CPROC));
	exit(0);
} 
