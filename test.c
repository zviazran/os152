#include "types.h"
#include "user.h"

void func(int *p)
{
	*p=0x7f;
}

int
main(int argc, char *argv[])
{
int pid;
int status;

func(&status);

if (!(pid = fork()))
{
	exit(0x7f);
}
else
{
	waitpid(pid,&status,0);
}
if (status == 0x7f)
{
	printf(1, "OK\n");
}
else
{
	printf(1, "FAILED\n");
}
 exit(0); //remove exit(0) to test 1.4 
  //return 0;
}