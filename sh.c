// Shell.

#include "types.h"
#include "user.h"
#include "fcntl.h"


// Parsed command representation
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 10

struct cmd {
  int type;
};

struct execcmd {
  int type;
  int index; // new in this code
  char *argv[MAXARGS];
  char *eargv[MAXARGS];
};

struct redircmd {
  int type;
  struct cmd *cmd;
  char *file;
  char *efile;
  int mode;
  int fd;
};

struct pipecmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct listcmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct backcmd {
  int type;
  struct cmd *cmd;
};

typedef struct job job; // job of process
typedef struct job_proc_node job_proc_node; // process of a job

int fork1(void);  // Fork but panics on failure.
void panic(char*);
struct cmd *parsecmd(char*);
int pipefd[2]; //the pipe used for the jobs
job * jobs=0; //head of the jobs

struct job_proc_node 
{
  char * name;
  int pid;
  job_proc_node * next;
};

struct job
{
  int Jid;
  char * name;
  job * next;
  job_proc_node * processes;
};


// ---------------------------------------------------- job manegment ------------------------------------------------------------------------------------------------------------------------
// delete processes from the job
void delete_died_procecess(job_proc_node ** head) {
	job_proc_node * current;
	job_proc_node * previous;
	job_proc_node * temp;
	if ((*head)) {      
		if ( !ispidalive((*head)->pid)) {			
			temp = *head;
			*head = (*head)->next;
			free(temp->name);
			free(temp);
		}
		current = (*head)->next;
		
		for(previous = *head;current && previous ;current = current->next){	  	
			if ( !ispidalive((*head)->pid)) {
				temp = current;
				previous->next = current->next;
				free(temp->name);
				free(temp);
			}
			previous = current;
		}
	}
}

// print Job all jobs 
void print_jobs(){
	job * temp;
	job_proc_node * process_list;
	int printJobs=0;

	// delete died procecess
	for(temp = jobs;temp;temp = temp->next)
		delete_died_procecess(&temp->processes);
	
	// print alive jobs	
	for(temp = jobs;temp;temp = temp->next)
		if (temp->processes ){
			printJobs=1;
			printf(2, "Job %d: ",temp->Jid);
			printf(2, "%s",temp->name);
			for(process_list=temp->processes;process_list;process_list = process_list->next){
				printf(2, "%d:",process_list->pid);  
				printf(2, " %s\n",process_list->name);  
			}
		}
	// print that there are no jobs			
	if (!printJobs) 
		printf(2,"There are no jobs\n");	
}

// add new job
job *append_job(job *job_list, job data){
	job * helperPointer = job_list;
	job * newjob = (job*)malloc(sizeof(job)); 
	newjob->name = data.name;
	newjob->Jid = data.Jid;
	newjob->processes = data.processes;
	newjob->next = 0;
	if(!job_list) 
		job_list = newjob;
	else {
		while (helperPointer->next){
			helperPointer = helperPointer->next;
		}
		helperPointer->next = newjob;
	}
	return job_list;
}

// add new job process
job_proc_node *append_process(job_proc_node *process_list, job_proc_node data){
	job_proc_node * helperPointer = process_list;
	job_proc_node * newProcess = (job_proc_node*)malloc(sizeof(job_proc_node)); 
	newProcess->name = data.name;
	newProcess->pid = data.pid;
	newProcess->next = 0;
	if(!process_list) 
		process_list = newProcess;
	else {
		while (helperPointer->next)
			helperPointer = helperPointer->next;
		helperPointer->next = newProcess;
	}
	return process_list;
}

// delete all jobs before exit
void delete_all_jobs(job *job_list){
	job * helper;
	job_proc_node* temp;
	for(helper = job_list;helper;helper = helper->next){
		for(temp = helper->processes;temp;helper->processes=temp){
			temp = helper->processes->next;
			free(helper->processes->name);
			free(helper->processes);
		}
	for(helper = job_list;helper;job_list=helper)
		helper = job_list->next;
		free(job_list->name);
		free(job_list);
	}
}

void bring_job_to_foreground(int jobid){
	job * temp;
	job_proc_node * process_list;
	int foundjob=0;

	// delete died procecess
	for(temp = jobs;temp;temp = temp->next)
		delete_died_procecess(&temp->processes);
	
	// print alive jobs
	for(temp = jobs;temp;temp = temp->next)
		if ((jobid==temp->Jid || !jobid) && temp->processes){
		  	foundjob=temp->Jid;
			for(process_list=temp->processes;process_list;process_list = process_list->next)
				waitpid(process_list->pid,0,0);
			break;
		}			
	if(foundjob)
		bring_job_to_foreground(foundjob);
}


// ----------------------------------------------------  job manegment  ------------------------------------------------------------------------------------------------------------------------


// Execute cmd.  Never returns.
void runcmd(struct cmd *cmd){
	int p[2];
	int pid;
	char name[16];
	struct backcmd *bcmd;
	struct execcmd *ecmd;
	struct listcmd *lcmd;
	struct pipecmd *pcmd;
	struct redircmd *rcmd;

	if(cmd == 0)
		exit(0);
	
	switch(cmd->type){
		default:
			panic("runcmd");

		case EXEC:
			ecmd = (struct execcmd*)cmd;
			if(ecmd->argv[0] == 0)
				exit(0);

			//add to job list
			pid = getpid();
			strcpy(name,ecmd->argv[0]);
			
			//write to the pipe
			close(pipefd[0]);
			write(pipefd[1],name,sizeof(name));            
			write(pipefd[1],&pid, sizeof(pid));
			close(pipefd[1]);
			
			exec(ecmd->argv[0], ecmd->argv);          
			printf(2, "exec %s failed\n", ecmd->argv[0]);
			break;

		case REDIR:
			rcmd = (struct redircmd*)cmd;
			close(rcmd->fd);
			if(open(rcmd->file, rcmd->mode) < 0){
				printf(2, "open %s failed\n", rcmd->file);
				exit(0);
			}
			runcmd(rcmd->cmd);
			break;

		case LIST:
			lcmd = (struct listcmd*)cmd;
			if(fork1() == 0)
				runcmd(lcmd->left);
			wait(0);
			runcmd(lcmd->right);
			break;

		case PIPE:
			pcmd = (struct pipecmd*)cmd;
			if(pipe(p) < 0)
				panic("pipe");
			if(fork1() == 0){
				close(1);
				dup(p[1]);
				close(p[0]);
				close(p[1]);
				runcmd(pcmd->left);
			}
			if(fork1() == 0){
				close(0);
				dup(p[0]);
				close(p[0]);
				close(p[1]);
				runcmd(pcmd->right);
			}
			close(p[0]);
			close(p[1]);
			close(pipefd[0]);
			close(pipefd[1]);
			wait(0);
			wait(0);
			break;
			
		case BACK:
			bcmd = (struct backcmd*)cmd;
			if(fork1() == 0)
				runcmd(bcmd->cmd);
			break;
	}
	exit(0);
}

int getcmd(char *buf, int nbuf){
	printf(2, "$ ");
	memset(buf, 0, nbuf);
	gets(buf, nbuf);
	if(buf[0] == 0) // EOF
		return -1;
	return 0;
}



int main(void){
	static char buf[100];
	int fd;
	char name[16];
	int pid;
	int jobCounter = 1; 
	job tempJob; //temporary single job
	job_proc_node tempJobProcHead;  // temporary single process
	job_proc_node * processes_list; //the list that will hild all processes belong to a single job

  
	// Assumes three file descriptors open.
	while((fd = open("console", O_RDWR)) >= 0){
		if(fd >= 3){
			close(fd);
			break;
		}
	}
  
	// Read and run input commands.
	while(getcmd(buf, sizeof(buf)) >= 0){
		if(buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' '){
			// Clumsy but will have to do for now.
			// Chdir has no effect on the parent if run in the child.
			buf[strlen(buf)-1] = 0;  // chop \n
			if(chdir(buf+3) < 0)
				printf(2, "cannot cd %s\n", buf+3);
			continue;
		}
  
		// check jobs -clumsy but will have to do for now.
		if(buf[0]=='j' && buf[1]=='o' && buf[2]=='b' && buf[3]=='s'){ 
			print_jobs();
			continue;
		}
		
		// cange to foreground -clumsy but will have to do for now.
		if(buf[0]=='f' && buf[1]=='g'){ 
			bring_job_to_foreground(atoi(((struct execcmd*)parsecmd(buf))->argv[1]));
			continue;
		}
		
		//define the pipe	
		pipe(pipefd);
		
		// run the command and son dosent return
		if(fork1() == 0)
			runcmd(parsecmd(buf));
		
		// make a new job
		tempJob.name = (char*)malloc(sizeof(buf)*sizeof(char));
		strcpy(tempJob.name,buf);
		tempJob.Jid = jobCounter++;
		
		// use the 0 side of the pipe to read	 from the process
		close(pipefd[1]);
		while (read(pipefd[0], name, sizeof(name)) >0){ 	// get process name
			read(pipefd[0], &pid, sizeof(pid)); 			// and process id

			//update new process
			tempJobProcHead.name = (char*)malloc(sizeof(name)*sizeof(char));
			strcpy(tempJobProcHead.name, name);
			tempJobProcHead.pid = pid;
			processes_list = append_process(processes_list, tempJobProcHead);
		}
		
		wait(0);
		
		//add job to list
		tempJob.processes = processes_list; 
		processes_list = 0;
		jobs = append_job(jobs, tempJob);
		}

	delete_all_jobs(jobs);
	exit(0);
}
    

void panic(char *s){
	printf(2, "%s\n", s);
	exit(1);
}

int fork1(void){
	int pid;
	
	pid = fork();
	if(pid == -1)
		panic("fork");
	return pid;
}

//PAGEBREAK!
// Constructors

struct cmd* execcmd(void){
	struct execcmd *cmd;

	cmd = malloc(sizeof(*cmd));
	memset(cmd, 0, sizeof(*cmd));
	cmd->type = EXEC;
	return (struct cmd*)cmd;
}

struct cmd*
redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd)
{
  struct redircmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = REDIR;
  cmd->cmd = subcmd;
  cmd->file = file;
  cmd->efile = efile;
  cmd->mode = mode;
  cmd->fd = fd;
  return (struct cmd*)cmd;
}

struct cmd*
pipecmd(struct cmd *left, struct cmd *right)
{
  struct pipecmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = PIPE;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
listcmd(struct cmd *left, struct cmd *right)
{
  struct listcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = LIST;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
backcmd(struct cmd *subcmd)
{
  struct backcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = BACK;
  cmd->cmd = subcmd;
  return (struct cmd*)cmd;
}
//PAGEBREAK!
// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&;()";

int
gettoken(char **ps, char *es, char **q, char **eq)
{
  char *s;
  int ret;
  
  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  if(q)
    *q = s;
  ret = *s;
  switch(*s){
  case 0:
    break;
  case '|':
  case '(':
  case ')':
  case ';':
  case '&':
  case '<':
    s++;
    break;
  case '>':
    s++;
    if(*s == '>'){
      ret = '+';
      s++;
    }
    break;
  default:
    ret = 'a';
    while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
      s++;
    break;
  }
  if(eq)
    *eq = s;
  
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return ret;
}

int
peek(char **ps, char *es, char *toks)
{
  char *s;
  
  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);
struct cmd *nulterminate(struct cmd*);

struct cmd* parsecmd(char *s){
	char *es;
	struct cmd *cmd;

	es = s + strlen(s);
	cmd = parseline(&s, es);
	peek(&s, es, "");
	if(s != es){
		printf(2, "leftovers: %s\n", s);
		panic("syntax");
	}
	nulterminate(cmd);
	return cmd;
}

struct cmd*
parseline(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
  while(peek(ps, es, "&")){
    gettoken(ps, es, 0, 0);
    cmd = backcmd(cmd);
  }
  if(peek(ps, es, ";")){
    gettoken(ps, es, 0, 0);
    cmd = listcmd(cmd, parseline(ps, es));
  }
  return cmd;
}

struct cmd*
parsepipe(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parseexec(ps, es);
  if(peek(ps, es, "|")){
    gettoken(ps, es, 0, 0);
    cmd = pipecmd(cmd, parsepipe(ps, es));
  }
  return cmd;
}

struct cmd*
parseredirs(struct cmd *cmd, char **ps, char *es)
{
  int tok;
  char *q, *eq;

  while(peek(ps, es, "<>")){
    tok = gettoken(ps, es, 0, 0);
    if(gettoken(ps, es, &q, &eq) != 'a')
      panic("missing file for redirection");
    switch(tok){
    case '<':
      cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
      break;
    case '>':
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    case '+':  // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    }
  }
  return cmd;
}

struct cmd*
parseblock(char **ps, char *es)
{
  struct cmd *cmd;

  if(!peek(ps, es, "("))
    panic("parseblock");
  gettoken(ps, es, 0, 0);
  cmd = parseline(ps, es);
  if(!peek(ps, es, ")"))
    panic("syntax - missing )");
  gettoken(ps, es, 0, 0);
  cmd = parseredirs(cmd, ps, es);
  return cmd;
}

struct cmd*
parseexec(char **ps, char *es)
{
  char *q, *eq;
  int tok, argc;
  struct execcmd *cmd;
  struct cmd *ret;
  
  if(peek(ps, es, "("))
    return parseblock(ps, es);

  ret = execcmd();
  cmd = (struct execcmd*)ret;

  argc = 0;
  ret = parseredirs(ret, ps, es);
  while(!peek(ps, es, "|)&;")){
    if((tok=gettoken(ps, es, &q, &eq)) == 0)
      break;
    if(tok != 'a')
      panic("syntax");
    cmd->argv[argc] = q;
    cmd->eargv[argc] = eq;
    argc++;
    if(argc >= MAXARGS)
      panic("too many args");
    ret = parseredirs(ret, ps, es);
  }
  cmd->argv[argc] = 0;
  cmd->eargv[argc] = 0;
  return ret;
}

// NUL-terminate all the counted strings.
struct cmd*
nulterminate(struct cmd *cmd)
{
  int i;
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if(cmd == 0)
    return 0;
  
  switch(cmd->type){
  case EXEC:
    ecmd = (struct execcmd*)cmd;
    for(i=0; ecmd->argv[i]; i++)
      *ecmd->eargv[i] = 0;
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    nulterminate(rcmd->cmd);
    *rcmd->efile = 0;
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    nulterminate(pcmd->left);
    nulterminate(pcmd->right);
    break;
    
  case LIST:
    lcmd = (struct listcmd*)cmd;
    nulterminate(lcmd->left);
    nulterminate(lcmd->right);
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    nulterminate(bcmd->cmd);
    break;
  }
  return cmd;
}
