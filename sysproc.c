#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}
int
sys_exit(void)
{
  int status;
  if(argint(0, &status) < 0) //pull arg from stack
    return -1;
  exit(status);
  return 0;  // not reached
}

int
sys_wait(void)
{
  int status;
  if(argint(0, &status) < 0)
    return -1;
  return wait((int*)status);
}

int
sys_waitpid(void)
{
  int pid,status,options;
  if((argint(0, &pid) || argint(1, &status) || argint(2, &options)) < 0)
    return -1;
  return waitpid(pid,(int*)status,options);
}

int
sys_wait_stat(void)
{
  int wtime,rtime,iotime;
  if((argint(0, &wtime) || argint(1, &rtime) || argint(2, &iotime)) < 0)
    return -1;
  return wait_stat((int*)wtime,(int*)rtime,(int*)iotime);
}

int
sys_ispidalive(void)
{
  int pid;
  if(argint(0, &pid) < 0)
    return -1;
  return ispidalive(pid);
}

int
sys_set_priority(void)
{
  int priority;
  if(argint(0, &priority) < 0)
    return -1;
  return set_priority(priority);
}


int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int sys_uptime(void){
	uint xticks;
	
	acquire(&tickslock);
	xticks = ticks;
	release(&tickslock);
	return xticks;
}
